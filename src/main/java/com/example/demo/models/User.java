package com.example.demo.models;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
public User() {
		super();
		// TODO Auto-generated constructor stub
	}

private double num;
public double getNum() {
	return num;
}
public void setNum(double num) {
	this.num = num;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDateTime() {
	return dateTime;
}
public void setDateTime(String dateTime) {
	this.dateTime = dateTime;
}
public String getVerifycode() {
	return Verifycode;
}
public void setVerifycode(String verifycode) {
	Verifycode = verifycode;
}
public String getClockInOut() {
	return clockInOut;
}
public void setClockInOut(String clockInOut) {
	this.clockInOut = clockInOut;
}
public double getDeviceId() {
	return deviceId;
}
public void setDeviceId(double deviceId) {
	this.deviceId = deviceId;
}
public String getDeviceName() {
	return deviceName;
}
public void setDeviceName(String deviceName) {
	this.deviceName = deviceName;
}
public String getUserExtFmt() {
	return userExtFmt;
}
public void setUserExtFmt(String userExtFmt) {
	this.userExtFmt = userExtFmt;
}
public String getError() {
	return error;
}
public void setError(String error) {
	this.error = error;
}

private String department;
private double id;
private String name;
private String dateTime;
private String Verifycode;
private String clockInOut;
private double deviceId;
private String deviceName;
private String userExtFmt;
private String error;

public double getId() {
	return id;
}
public void setId(double id) {
	this.id = id;
}
public User(double num, String department,double id, String name, String dateTime, String verifycode, String clockInOut,
		double deviceId, String deviceName, String userExtFmt) {
	super();
	this.num = num;
	this.department = department;
	this.name = name;
	this.dateTime = dateTime;
	Verifycode = verifycode;
	this.clockInOut = clockInOut;
	this.deviceId = deviceId;
	this.deviceName = deviceName;
	this.userExtFmt = userExtFmt;
	this.id=id;
}
public static User createAlert(String errorMessage) {
    User user= new User();
    return user.alert(errorMessage);
  
 }

 public User alert(String errorMessage) {
    error = errorMessage;
    return this;
 }


}
