package com.example.demo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadEvent {
private String eventType="progress";
private Object state;
public Object getState() {
	return state;
}
public void setState(Object state) {
	this.state = state;
}
public void setEventType(String eventType) {
	this.eventType = eventType;
}
public String getEventType() {
	return eventType;
}
}
