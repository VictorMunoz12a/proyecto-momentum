package com.example.demo.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo.models.User;
import com.example.demo.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class UserController {

	@Autowired 
	private UserService userService;
	
	@RequestMapping(value="/erp/get-users",method = RequestMethod.GET)
		public @ResponseBody List<User> getUsers(){
		return userService.findAll();
	}
	
	@RequestMapping(value="/erp/get-users",method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> countMomentums(){
		Map<String, Integer> result= new HashMap<>();
		return result;
	}
}
