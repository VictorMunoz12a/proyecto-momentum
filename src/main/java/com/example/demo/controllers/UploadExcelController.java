package com.example.demo.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import com.example.demo.models.UploadEvent;
import com.example.demo.models.User;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.demo.services.UserService;
import com.example.demo.utils.DataRow;
import com.example.demo.utils.DataTable;
import com.example.demo.utils.ExcelTable;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public class UploadExcelController {
	private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

	   private static final Logger logger = LoggerFactory.getLogger(UploadExcelController.class);

	   @Autowired
	   private UserService userService;
	   @Autowired
	   private SimpMessagingTemplate brokerMessagingTemplate;

	   private String getUser(String token) {
		   return "user";
	   }
	   
	   
	   @RequestMapping(value = "/erp/upload-excel", method = RequestMethod.POST)
	   public @ResponseBody Map<String, Object> uploadProductCsv(
	           @RequestParam("file") MultipartFile file,
	           @RequestParam("token") String token)
	           throws ServletException, IOException {
	      logger.info("upload-excel invoked.");
	   
	      Map<String, Object> result = new HashMap<>();

	      final String label = UUID.randomUUID().toString() + ".xlsx";
	      final String filepath = "/tmp/" + label;
	      byte[] bytes = file.getBytes();
	      File fh = new File("/tmp/");
	      if(!fh.exists()){
	         fh.mkdir();
	      }
	      try {

	          FileOutputStream writer = new FileOutputStream(filepath);
	          writer.write(bytes);
	          writer.close();

	          logger.info("image bytes received: {}", bytes.length);

	          executor.submit(() -> {
	              try {
	         UploadEvent u= new UploadEvent();
	         u.setState("Archivo cargado y recivido en el servidor");
	        u.setEventType("start");
	        brokerMessagingTemplate.convertAndSend("/topics/event", JSON.toJSONString(u, SerializerFeature.BrowserCompatible));

	        
	        final FileInputStream inputStream = new FileInputStream(filepath);
            DataTable table = ExcelTable.load(() -> inputStream);

            int rowCount = table.rowCount();

            for(int i=0; i < rowCount; ++i) {
               DataRow row = table.row(i);

               String num = row.cell("Num");
               String department = row.cell("Department");
               String  name= row.cell("Name");
               String id = row.cell("ID");
               String dateTime = row.cell("Date/Time");
               String verifyCode = row.cell("Verifycode");
               String clock = row.cell("Clock-in/out");
               String deviceId = row.cell("DeviceID");
               String deviceName = row.cell("DeviceName");
               String userExtFmt = row.cell("UserExtFmt");
              

               User product = new User();
               product.setNum(Double.parseDouble(num));
               product.setDepartment(department);
               product.setName(name);
             product.setId(Double.parseDouble(id));
             product.setDateTime(dateTime);
             product.setVerifycode(verifyCode);
             product.setClockInOut(clock);
             product.setDeviceId(Double.parseDouble(deviceId));
             product.setDeviceName(deviceName);
             product.setUserExtFmt(userExtFmt);
              

               logger.info("Saving product: {}", product.getName());
               userService.saveUser(product);

               u = new UploadEvent();
               u.setState(product);
               u.setEventType("progress");
               brokerMessagingTemplate.convertAndSend("/topics/event", JSON.toJSONString(u, SerializerFeature.BrowserCompatible));

               Thread.sleep(5000L);
            }


            u = new UploadEvent();
            u.setState("Uploaded filed deleted on server");
            fh.delete();
            u.setEventType("end");
            brokerMessagingTemplate.convertAndSend("/topics/event", JSON.toJSONString(u, SerializerFeature.BrowserCompatible));

         }catch(Exception ex) {
            logger.error("Failed on saving product", ex);
         }
      });

      result.put("success", true);
      result.put("id", label);
      result.put("error", "");

      return result;
   }catch(IOException ex) {
      logger.error("Failed to process the uploaded image", ex);
      result.put("success", false);
      result.put("id", "");
      result.put("error", ex.getMessage());
      return result;
   }

}
}

